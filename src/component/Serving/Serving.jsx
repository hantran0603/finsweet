import React from "react";
import { FaBirthdayCake, FaCocktail } from "react-icons/fa";
import { CgGlassAlt } from "react-icons/cg";
import { MdFastfood } from "react-icons/md";
import { GiChickenLeg } from "react-icons/gi";
import { RiCake3Fill } from "react-icons/ri";
import Container from "../ui/Container";
import Introduction from "../common/Introduction";

const Serving = () => {
  return (
    <Container>
      <div className="pb-10 lg:pb-[160px] h-full">
        <Introduction
          title="WHAT WE ARE SERVING"
          heading="We all have to eat, so why not do it beautifully?"
          content=" Through True Rich Attended does no end it his mother since
        favourable real had half every him case in packages enquire we up
        ecstatic.. Through True Rich Attended does no end it his mother"
        />
     
        <div className="grid h-full grid-cols-1 md:grid-cols-3 bg-[#050606]">
          <div className="h-full flex flex-col">
            <div className=" p-10 sm:p-[64px] border-none grow sm:border-r border-[#95989d]">
              <div className="w-[48px] h-[48px] rounded-full text-link flex justify-center items-center bg-[#ffb57712] mb-[24px] ">
                <FaBirthdayCake />
              </div>
              <h3 className="font-playfair font-bold text-32 pb-[12px]">
                Quafe Cake
              </h3>
              <p className="font-normal text-graya ">
                Through True Rich Attended no end it his mother since favourable
                real had half every him.
              </p>
            </div>

            <div className=" p-10 sm:p-[64px] border-none sm:border-r border-[#95989d]">
              <div className="w-[48px] h-[48px] rounded-full text-link flex justify-center items-center bg-[#ffb57712] mb-[24px] ">
                <FaCocktail />
              </div>
              <h3 className="font-playfair font-bold text-32 pb-[12px]">
                Cocktails
              </h3>
              <p className="font-normal text-graya ">
                Through True Rich Attended no end it his mother since favourable
                real had half every him.
              </p>
            </div>
          </div>

          <div className="h-full flex flex-col">
            <div className=" p-10 sm:p-[64px] border-none  grow sm:border-r border-[#95989d]">
              <div className="w-[48px] h-[48px] rounded-full text-link flex justify-center items-center bg-[#ffb57712] mb-[24px] ">
                <CgGlassAlt />
              </div>
              <h3 className="font-playfair font-bold text-32 pb-[12px]">
                Cofee
              </h3>
              <p className="font-normal text-graya ">
                Through True Rich Attended no end it his mother since favourable
                real had half every him.
              </p>
            </div>
            <div className=" p-10 sm:p-[64px] border-none sm:border-r border-[#95989d]">
              <div className="w-[48px] h-[48px] rounded-full text-link flex justify-center items-center bg-[#ffb57712] mb-[24px] ">
                <GiChickenLeg />
              </div>
              <h3 className="font-playfair font-bold text-32 pb-[12px]">
                Grill
              </h3>
              <p className="font-normal text-graya ">
                Through True Rich Attended no end it his mother since favourable
                real had half every him.
              </p>
            </div>
          </div>

          <div className="h-full flex flex-col">
            <div className=" p-10 sm:p-[64px] grow">
              <div className="w-[48px] h-[48px] rounded-full text-link flex justify-center items-center bg-[#ffb57712] mb-[24px] ">
                <MdFastfood />
              </div>
              <h3 className="font-playfair font-bold text-32 pb-[12px]">
                Fast Food
              </h3>
              <p className="font-normal text-graya ">
                Through True Rich Attended no end it his mother since favourable
                real had half every him.
              </p>
            </div>
            <div className=" p-10 sm:p-[64px]">
              <div className="w-[48px] h-[48px] rounded-full text-link flex justify-center items-center bg-[#ffb57712] mb-[24px] ">
                <RiCake3Fill />
              </div>
              <h3 className="font-playfair font-bold text-32 pb-[12px]">
                Snacks
              </h3>
              <p className="font-normal text-graya ">
                Through True Rich Attended no end it his mother since favourable
                real had half every him.
              </p>
            </div>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default Serving;
