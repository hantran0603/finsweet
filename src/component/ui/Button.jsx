import React from "react";
import { HiArrowLongRight } from "react-icons/hi2";

const Button = (props) => {
  return (
    <a
      href="# "
      className="flex py-3 font-medium justify-center  w-40 bg-link text-[#2E2D33] mb-10 hover:bg-[#ffbb81]"
    >
      {props.content}
      <span className="ml-3 mt-1">
        <HiArrowLongRight />
      </span>
    </a>
  );
};

export default Button;
