import React from 'react'

const Product = (props) => {
  return (
    <div>
      <img className="w-full" src={props.src} alt=""></img>
      <div className="p-6 bg-[#1F1D21] flex flex-wrap justify-between hover:bg-[#2b292d] cursor-pointer">
        <h4 className="font-bold text-20 font-playfair ">{props.name}</h4>
        <p className="font-medium opacity-[0.87] text-20 ">{props.price}</p>
      </div>
    </div>
  )
}

export default Product