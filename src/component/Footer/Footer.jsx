import React from "react";
import Container from "../ui/Container";
import { BsFacebook } from "react-icons/bs";
import { CiTwitter } from "react-icons/ci";
import { AiOutlineInstagram, AiFillLinkedin } from "react-icons/ai";
import { HiArrowLongRight } from "react-icons/hi2";

const Footer = () => {
  return (
    <Container>
      <div>
        <div className="flex-col sm:flex-row flex justify-center pb-11">
          <a href="# " className="text-link font-bold mr-6 text-20 text-center hover:text-[#ebc19f] ">
            {"{"}Finsweet
          </a>
        </div>
        <div className=" sm:pb-8">
          <ul className="hidden sm:flex justify-center gap-x-10 sm:gap-x-9 font-medium ">
            <li>
              <a className="hover:text-gray-500" href="# ">Home</a>
            </li>
            <li>
              <a className="hover:text-gray-500" href="# ">Menu</a>
            </li>
            <li>
              <a className="hover:text-gray-500" href="# ">About Us</a>
            </li>
            <li>
              <a className="hover:text-gray-500" href="# ">Our Story</a>
            </li>
            <li>
              <a className="hover:text-gray-500" href="# ">Blog</a>
            </li>
            <li>
              <a className="hover:text-gray-500" href="# ">Contact us</a>
            </li>
          </ul>
        </div>
        <div className="py-6 flex-col sm:grid sm:grid-cols-3 border-t border-opacity-[0.07]">
          <div className="text-center sm:flex sm:justify-start">
            <span className="font-normal text-graya capitalize">
              © 2021 Finsweet | All rights reserved
            </span>
          </div>
          <div className="flex justify-center">
            <ul className="text-18 flex gap-6">
              <li>
                <a className="hover:text-gray-500" href="# " alt="">  <BsFacebook /></a>
               
              </li>
              <li>
              <a className="hover:text-gray-500" href="# " alt="">  <CiTwitter /></a>
               
              </li>
              <li>
              <a className="hover:text-gray-500" href="# " alt=""> <AiOutlineInstagram /> </a>
               
              </li>
              <li>
              <a className="hover:text-gray-500" href="# " alt=""><AiFillLinkedin /> </a>
                
              </li>
            </ul>
          </div>
          <div className="sm:flex sm:justify-end">
            <div className=" sm:block md:ml-auto flex justify-center">
              <a href="# " className=" flex hover:text-gray-500">
                Contact Us
                <span className="ml-3 mt-1 text-white ">
                  <HiArrowLongRight className="text-link"/>
                </span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default Footer;
