import React from "react";
import { FaHandPointDown } from "react-icons/fa";
import knowMore1Img from "../../images/knowmore1.png";
import knowMore2Img from "../../images/knowmore2.png";
import Container from "./../ui/Container";
import Button from "../ui/Button";
import IntroductionBlock from "../common/IntroductionBlock";

const KnowMore = () => {
  return (
    <Container>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-4 pb-10 lg:pb-[160px]">
        <div>
          <IntroductionBlock
            title="KNOW MORE ABOUT US"
            heading="We source sustainable & line caught Foods"
            content="Edit this text to make it your own. To edit, simply click directly
          on the text to start adding your own words. You can move the text by
          dragging and dropping the text anywhere on the page."
          />
      
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 pt-[40px] text-graya font-normal pb-[32px] gap-5">
            <div>
              <span className="font-medium text-24 text-[#C4E4EA] flex">
                <span className="text-link pr-2">
                  <FaHandPointDown />
                </span>
                10+ People
              </span>
              <p className="pb-3.5">We are Small Team</p>
              <p>
                Through True Rich Attended does no end it his mother since
                favourable.
              </p>
            </div>
            <div>
              <span className="font-medium text-24 text-[#C4E4EA] flex">
                <span className="text-link pr-2">
                  <FaHandPointDown />
                </span>
                2014
              </span>
              <p className="pb-3.5">We are From</p>
              <p>
                Through True Rich Attended does no end it his mother since
                favourable.
              </p>
            </div>
            <div>
              <span className="font-medium text-24 text-[#C4E4EA] flex">
                <span className="text-link pr-2">
                  <FaHandPointDown />
                </span>
                200k
              </span>
              <p className="pb-3.5">We Served</p>
              <p>
                Through True Rich Attended does no end it his mother since
                favourable.
              </p>
            </div>
          </div>

          <Button content="View Menu" />
        </div>
        <div className="sm:grid grid-cols-1 sm:grid-cols-2  ml-auto sm:gap-[67px] gap-14 flex">
          <div className="pt-0 sm:pt-[73px]">
            <img src={knowMore1Img} alt=""></img>
          </div>
          <div>
            <img src={knowMore2Img} alt=""></img>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default KnowMore;
