import React from "react";
import Container from "../ui/Container";
import mapImg from "../../images/map.png";

const Contact = () => {
  return (
    <Container>
      <div className="flex h-full flex-col md:flex-row pb-10 lg:pb-40">
        <div className="py-20 px-16 flex-1  bg-[#1F1D21]">
          <div className="pb-8">
            <h4 className="font-bold text-20 font-playfair pt-1.5">
              Working Hour
            </h4>
            <span className="font-normal text-graya">
              Sunday to Saturday
              <br />
              09:00 AM to 11:00 PM
            </span>
          </div>
          <div className="pb-8">
            <h4 className="font-bold text-20 font-playfair pt-1.5">Location</h4>
            <span className="font-normal text-graya">
              Street - 127, New York
              <br />
              United States
              <br />
              546544
            </span>
          </div>
          <div>
            <h4 className="font-bold text-20 font-playfair pt-1.5">Location</h4>
            <span className="font-normal text-graya">
              +123456789
              <br />
              hey@yourdomain.com
            </span>
          </div>
        </div>
        <div className="w-full flex-[2]">
          <img className="w-full h-full" src={mapImg} alt="" />
        </div>
      </div>
    </Container>
  );
};

export default Contact;
