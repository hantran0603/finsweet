import React from "react";
import quoteImg from "../../images/quote.png";
import imageAvt from "../../images/avt.png";
import Container from "../ui/Container";

const Quote = () => {
  return (
    <div className="bg-[#1F1D21] ">
      <Container className="md:mr-0 md:px-0">
        <div className="w-full h-full flex flex-col lg:flex-row">
          <div className="flex-1 md:pr-16 w-full sm:px-2.5 py-4 sm:py-20 ">
            <span className="text-72">“</span>
            <p className="font-medium text-32 font-playfair opacity-[0.87] ]">
              Edit this text to make it your own. To edit, simply click directly
              on the text to start adding your own words. You can move the text
              by dragging and dropping the text
            </p>
            <div className="pt-16 border-b flex  justify-between">
              <div>
                <p className="font-medium">Joheny Andro</p>
                <span className="text-12 opacity-[0.6]">
                  Bhubaneswar, Odisha
                </span>
              </div>
              <div>
                <img className="pb-5" src={imageAvt} alt=""></img>
                <hr />
              </div>
            </div>
          </div>
          <div className=" w-full flex-[2] ">
            <img className="w-full h-full" src={quoteImg} alt=""></img>
          </div>
        </div>
      </Container>
    </div>
  );
  
};

export default Quote;
