import React from "react";

const BlockNews = (props) => {
  return (
    <div className="flex flex-col h-full">
      <div className="p-8 bg-[#1F1D21] grow hover:bg-[#414347] cursor-pointer">
        <span className="font-normal text-graya text-14 pb-2">
          {props.date}
        </span>
        <h3 className="font-playfair text-32 font-bold opacity-[0.87] pb-4">
          {props.heading}
        </h3>
        <span className="text-graya font-normal">{props.content}</span>
      </div>
      <img src={props.src} alt="image_news"></img>
    </div>
  );
};

export default BlockNews;
