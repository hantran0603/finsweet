import React from "react";
import Container from "../ui/Container";
import coffeeImg from "../../images/coffe.png";
import Burger from "../../images/burger.png";
import Choco from "../../images/choco.png";
import Fries from "../../images/fries.png";
import Sandwitch from "../../images/sandwitch.png";
import Soup from "../../images/soup.png";
import Product from "../Product/Product";
import Button from "../ui/Button";
import Introduction from "../common/Introduction";
import GridCard from "../ui/GridCard";

const OurMenu = () => {
  return (
    <Container>
      <div className="pb-10 lg:pb-40">
        <Introduction
          title="OUR MENU"
          heading="Discover our menu chart"
          text="Most Popular Picks"
          content="Through True Rich Attended does no end it his mother since
        favourable real had half every him case in packages enquire we up
        ecstatic.. Through True Rich Attended does no end it his mother"
        />
        <GridCard>
          <Product src={coffeeImg} name="Drp Cofee" price="$08.85" />
          <Product src={Burger} name="Chicken Burger" price="$08.85" />
          <Product src={Choco} name="Choco Cup Cake" price="$4.85" />
          <Product src={Fries} name="French Fries" price="$7.85" />
          <Product src={Sandwitch} name="Sandwitch" price="$3.97" />
          <Product src={Soup} name="Chinese Soup" price="$8.85" />
        </GridCard>

        <div className="flex justify-center">
          <Button content="View Menu" />
        </div>
      </div>
    </Container>
  );
};

export default OurMenu;
