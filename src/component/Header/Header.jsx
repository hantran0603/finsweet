import React from "react";
import imageHeader from "../../images/image-header.png";
import Container from "../ui/Container";
import Button from "../ui/Button";

const Header = () => {
  return (
    <Container>
      <div className="grid grid-cols-1 gap-2 lg:grid-cols-2 pb-10 lg:pb-[160px] ">
        <div className="pt-3 sm:pt-[50px]">
          <h1 className="font-playfair text-56 mb-2 opacity-[0.87] mt-18">
            We serve high quality foods of all kinds.
          </h1>
          <p className="font-normal text-graya mb-8">
            Edit this text to make it your own. To edit, simply click directly
            on the text to start adding your own words. You can move the text.
          </p>

          <Button content="View Menu" />
          <div className="mb-4">
            <h4 className="font-bold font-playfair">Opening Times</h4>
            <p className="opacity-[0.6] font-normal">
              Sunday to Saturday l 09:00 AM to 11:00 PM
            </p>
          </div>

          <div className="mb-4">
            <h4 className="font-bold font-playfair">Location</h4>
            <p className="opacity-[0.6] font-normal">
              Master canteen, BBSR , Odisha 752054
            </p>
          </div>
          <div>
            <h4 className="font-bold font-playfair">Call us</h4>
            <p className="opacity-[0.6] font-normal">+9776462441</p>
          </div>
        </div>

        <div className="mx-auto lg:ml-auto">
          <img src={imageHeader} alt="ImageHeader"></img>
        </div>
      </div>
    </Container>
  );
};

export default Header;
