import React from "react";
import { HiArrowLongRight } from "react-icons/hi2";
import { AiOutlineMenu } from "react-icons/ai";
import Container from "../ui/Container";
import { useState } from "react";
import NavbarModal from "../common/NavbarModal";
import { AiFillCloseSquare } from "react-icons/ai";

const NavBar = () => {
  const [open, setOpen] = useState(false);
  return (
  
   <Container >
      <nav className="flex lg:items-center  md:py-[33px] lg:mb-[96px] relative ">
        <a
          href="index.html"
          className="text-link font-bold mr-6 lg:text-18 hover:text-[#ebc19f] px-2 mt-4 md:px-0 md:mt-0 md:relative text-24"
        >
          {"{"}Finsweet
        </a>

        <ul className="hidden lg:flex sm:gap-x-2 md:gap-x-8 font-medium ">
          <li>
            <a className="hover:text-gray-500" href="# ">
              Home
            </a>
          </li>
          <li>
            <a className="hover:text-gray-500" href="# ">
              Menu
            </a>
          </li>
          <li>
            <a className="hover:text-gray-500" href="# ">
              About Us
            </a>
          </li>
          <li>
            <a className="hover:text-gray-500" href="# ">
              Our Story
            </a>
          </li>
          <li>
            <a className="hover:text-gray-500" href="# ">
              Blog
            </a>
          </li>
          <li>
            <a className="hover:text-gray-500" href="# ">
              Contact
            </a>
          </li>
        </ul>

        <div className="lg:hidden ml-auto flex justify-center pt-4 md:pt-0">
          <a
            onClick={() => setOpen(!open)}
            className={`"text-link  flex hover:text-[#ebc19f]  lg:hidden ml-auto ${
              open ? "hidden" : "block"
            } `}
            href="# "
          >
            <span className="ml-3 mt-1 text-white ">
              <AiOutlineMenu className="text-32 text-link" />
            </span>
          </a>
          <div className={`flex justify-center ${open ? "block" : "hidden"}`}>
            <button
              className="modal-close text-link"
              type="button"
              onClick={() => setOpen(false)}
            >
              <AiFillCloseSquare className="text-32 sm:text-40" />
            </button>
          </div>
        </div>

        <div className="hidden lg:block md:ml-auto">
          <a href="# " className="text-link flex hover:text-[#ebc19f] ">
            Clone Project
            <span className="ml-3 mt-1 text-white ">
              <HiArrowLongRight />
            </span>
          </a>
        </div>
      </nav>
      <NavbarModal open={open} setOpen={setOpen}></NavbarModal>
    </Container>
 
  );
};

export default NavBar;
