import React from "react";
import { HiArrowLongRight } from "react-icons/hi2";

const NavbarModal = ({ open, setOpen }) => {
  const content = open && (
    
    <div className="mx-auto z-10 clear-both float-none ">
      <div className="modal relative justify-center flex ">
        <div className="fixed w-full z-10  px-2 ">
        <ul className="gap-y-2 sm:gap-y-8 text-20 text-center font-medium flex  flex-col sm:py-4  bg-link px-2 ">
          <li>
            <a className="hover:text-gray-500" href="# ">
              Home
            </a>
          </li>
          <li>
            <a className="hover:text-gray-500" href="# ">
              Menu
            </a>
          </li>
          <li>
            <a className="hover:text-gray-500" href="# ">
              About Us
            </a>
          </li>
          <li>
            <a className="hover:text-gray-500" href="# ">
              Our Story
            </a>
          </li>
          <li>
            <a className="hover:text-gray-500" href="# ">
              Blog
            </a>
          </li>
          <li>
            <a className="hover:text-gray-500" href="# ">
              Contact
            </a>
          </li>
          <li className="flex justify-center">
          <a href="# " className=" flex hover:text-[#ebc19f] ">
            Clone Project
            <span className="ml-3 mt-1 text-white ">
              <HiArrowLongRight />
            </span>
          </a>
          </li>
        </ul>
        </div>
      
      </div>
    </div>
  );
  return content;
};

export default NavbarModal;
