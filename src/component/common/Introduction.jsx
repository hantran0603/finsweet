import React from "react";
import { clsx } from "clsx";

const Introduction = (props) => {
  return (
    <div>
      <div className="flex">
        <p className="text-link font-playfair font-normal ">{props.title}</p>
        <span className={clsx(props.classhr, "flex items-center pl-4")}>
          <hr className="w-12" />
        </span>
      </div>
      <div className={clsx(props.classblock," grid grid-cols-1 md:grid-cols-2 gap-[50px] md:pb-[50px]")}>
        <div>
          <h2 className="font-playfair font-bold text-48 opacity-[0.87]">
            {props.heading}
          </h2>
          <h5 className="font-bold text-14 text-[#C4E4EA] font-playfair pt-0 sm:pt-8">
            {props.text}
          </h5>
        </div>
        <div>
          <p className="font-normal text-graya pt-0 sm:pt-[28px] pb-10">
            {props.content}
          </p>
        </div>
      </div>
    </div>
  );
};

export default Introduction;
