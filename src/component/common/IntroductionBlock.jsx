import React from "react";
import { clsx } from "clsx";

const IntroductionBlock = (props) => {
  return (
    <div>
      <div className="sm:pb-[20px] flex ">
        <p className="text-link font-playfair font-normal ">{props.title}</p>
        <span className="flex items-center pl-4">
          <hr className={clsx(props.classhr, "w-12")} />
        </span>
      </div>
      <h1 className="font-playfair text-56 mb-2 opacity-[0.87] mt-18">
        {props.heading1}
      </h1>
      <h2 className="pb-8 font-bold font-playfair text-48">{props.heading}</h2>
      <p className="text-graya font-normal">{props.content}</p>
    </div>
  );
};

export default IntroductionBlock;
