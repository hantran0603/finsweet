import React from "react";
import Container from "../ui/Container";
import GridCard from "../ui/GridCard";
import news1Img from "../../images/newscf.png";
import news2Img from "../../images/newscake.png";
import news3Img from "../../images/news3.png";
import BlockNews from "../BlockNews/BlockNews";
import IntroductionBlock from "./../common/IntroductionBlock";

const Blog = () => {
  return (
    <Container>
      <div className="py-10 lg:py-40">
        <div>
          <IntroductionBlock
            heading="Read Our Lastest Blog"
            classhr="hidden"
            classblock="flex"
          />
        </div>
        <div>
          <GridCard>
            <BlockNews
              date="21 Jun 2021"
              heading="Extra Thick Homemad Pizza Sauce"
              content="It is a long established fact that a reader will be distracted by the readable content of a page."
              src={news1Img}
            />
            <BlockNews
              date="21 Jun 2021"
              heading="The Best Way to Store Fresh Herbs"
              content="It is a long established fact that a reader will be distracted by the readable content of a page."
              src={news2Img}
            />
            <BlockNews
              date="21 Jun 2021"
              heading="5 ways to prepare porridge"
              content="It is a long established fact that a reader will be distracted by the readable content of a page."
              src={news3Img}
            />
          </GridCard>
        </div>
      </div>
    </Container>
  );
};

export default Blog;
