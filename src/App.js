import NavBar from './component/NavBar/NavBar';
import Header from './component/Header/Header';
import KnowMore from './component/KnowMore/KnowMore';
import Serving from './component/Serving/Serving';
import OurMenu from './component/OurMenu/OurMenu';
import Quote from './component/Quote/Quote';
import Blog from './component/Blog/Blog';
import Contact from './component/Contact/Contact';
import Footer from './component/Footer/Footer';

function App() {
  return (
    <div className='bg-main font-poppins text-white'>
      <NavBar />
      <Header />
      <KnowMore />
      <Serving />
      <OurMenu />
      <Quote />
      <Blog />
      <Contact />
      <Footer/>
    </div>
  );
}

export default App;
