/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    fontFamily: {
      playfair: ["Playfair Display"],
      poppins: ["Poppins"],
    },
    extend: {
      fontSize: {
        10: "10px",
        12: "12px",
        13: "13px",
        14: "14px",
        16: "16px",
        18: "18px",
        20: "20px",
        22: "22px",
        24: "24px",
        26: "26px",
        28: "28px",
        30: "30px",
        32: "32px",
        34: "34px",
        36: "36px",
        38: "38px",
        40: "40px",
        42: "42px",
        44: "44px",
        46: "46px",
        48: "48px",
        50: "50px",
        56: "56px",
        58: "58px",
        60: "60px",
        62: "62px",
        72: "72px",
      },
      colors: {
        link: '#FFB577',
        main: '#19161B',
        graya: '#B8B8B8',
      },
      flexGrow: {
        '2': 2
      },
    },
  },
  plugins: [],
}
